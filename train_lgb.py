import lightgbm as lgb
import numpy as np
import pandas as pd
import xgboost as xgb
from sklearn.model_selection import KFold

############# READING DATA ################

print('Reading train.csv file ...')
train = pd.read_csv('data-science-bowl-2019/train.csv')
print('Training.csv file have {} rows and {} columns'.format(train.shape[0], train.shape[1]))
print('Reading test.csv file ...')
test = pd.read_csv('data-science-bowl-2019/test.csv')
print('Test.csv file have {} rows and {} columns'.format(test.shape[0], test.shape[1]))
print('Reading train_labels.csv file ...')
train_labels = pd.read_csv('data-science-bowl-2019/train_labels.csv')
print('Train_labels.csv file have {} rows and {} columns'.format(train_labels.shape[0], train_labels.shape[1]))
print('Reading specs.csv file ...')
specs = pd.read_csv('data-science-bowl-2019/specs.csv')
print('Specs.csv file have {} rows and {} columns'.format(specs.shape[0], specs.shape[1]))
print('Reading sample_submission.csv file ...')
sample_submission = pd.read_csv('data-science-bowl-2019/sample_submission.csv')
print('sample_submission.csv file has {} rows and {} columns'.format(sample_submission.shape[0],
                                                                     sample_submission.shape[1]))


def get_time(df):
    df['timestamp'] = pd.to_datetime(df['timestamp'])
    df['date'] = df['timestamp'].dt.date
    df['month'] = df['timestamp'].dt.month
    df['hour'] = df['timestamp'].dt.hour
    df['dayofweek'] = df['timestamp'].dt.dayofweek
    return df


def get_object_columns(df, columns):
    df = df.groupby(['installation_id', columns])['event_id'].count().reset_index()
    df = df.pivot_table(index='installation_id', columns=[columns], values='event_id')
    df.columns = list(df.columns)
    df.fillna(0, inplace=True)
    return df


def get_numeric_columns(df, column):
    df = df.groupby('installation_id').agg({f'{column}': ['mean', 'sum', 'std']})
    df.fillna(0, inplace=True)
    df.columns = [f'{column}_mean', f'{column}_sum', f'{column}_std']
    return df


def get_numeric_columns_2(df, agg_column, column):
    df = df.groupby(['installation_id', agg_column]).agg({f'{column}': ['mean', 'sum', 'std']}).reset_index()
    df = df.pivot_table(index='installation_id', columns=[agg_column],
                        values=[col for col in df.columns if col not in ['installation_id', 'type']])
    df.fillna(0, inplace=True)
    df.columns = list(df.columns)
    return df


numerical_columns = ['game_time']
categorical_columns = ['type', 'world']

reduce_train = pd.DataFrame({'installation_id': train['installation_id'].unique()})
reduce_train.set_index('installation_id', inplace=True)
reduce_test = pd.DataFrame({'installation_id': test['installation_id'].unique()})
reduce_test.set_index('installation_id', inplace=True)

train = get_time(train)

for i in numerical_columns:
    reduce_train = reduce_train.merge(get_numeric_columns(train, i), left_index=True, right_index=True)
    reduce_test = reduce_test.merge(get_numeric_columns(test, i), left_index=True, right_index=True)

for i in categorical_columns:
    reduce_train = reduce_train.merge(get_object_columns(train, i), left_index=True, right_index=True)
    reduce_test = reduce_test.merge(get_object_columns(test, i), left_index=True, right_index=True)

for i in categorical_columns:
    for j in numerical_columns:
        reduce_train = reduce_train.merge(get_numeric_columns_2(train, i, j), left_index=True, right_index=True)
        reduce_test = reduce_test.merge(get_numeric_columns_2(test, i, j), left_index=True, right_index=True)

reduce_train.reset_index(inplace=True)
reduce_test.reset_index(inplace=True)

print('Our training set have {} rows and {} columns'.format(reduce_train.shape[0], reduce_train.shape[1]))

# get the mode of the title
labels_map = dict(train_labels.groupby('title')['accuracy_group'].agg(lambda x: x.value_counts().index[0]))
# merge target
labels = train_labels[['installation_id', 'title', 'accuracy_group']]
# replace title with the mode
labels['title'] = labels['title'].map(labels_map)
# get title from the test set
reduce_test['title'] = test.groupby('installation_id').last()['title'].map(labels_map).reset_index(drop=True)
# join train with labels
reduce_train = labels.merge(reduce_train, on='installation_id', how='left')
print('We have {} training rows'.format(reduce_train.shape[0]))

categoricals = ['title', 'Activity', 'Assessment', 'Clip', 'Game', 'CRYSTALCAVES', 'MAGMAPEAK', 'NONE', 'TREETOPCITY']
reduce_train = reduce_train[
    ['installation_id', 'game_time_mean', 'game_time_sum', 'game_time_std', 'Activity', 'Assessment',
     'Clip', 'Game', 'CRYSTALCAVES', 'MAGMAPEAK', 'NONE', 'TREETOPCITY', ('game_time', 'mean', 'Activity'),
     ('game_time', 'mean', 'Assessment'), ('game_time', 'mean', 'Clip'), ('game_time', 'mean', 'Game'),
     ('game_time', 'std', 'Activity'), ('game_time', 'std', 'Assessment'), ('game_time', 'std', 'Clip'),
     ('game_time', 'std', 'Game'), ('game_time', 'sum', 'Activity'), ('game_time', 'sum', 'Assessment'),
     ('game_time', 'sum', 'Clip'), ('game_time', 'sum', 'Game'), ('game_time', 'mean', 'CRYSTALCAVES'),
     ('game_time', 'mean', 'MAGMAPEAK'), ('game_time', 'mean', 'NONE'), ('game_time', 'mean', 'TREETOPCITY'),
     ('game_time', 'std', 'CRYSTALCAVES'), ('game_time', 'std', 'MAGMAPEAK'), ('game_time', 'std', 'NONE'),
     ('game_time', 'std', 'TREETOPCITY'), ('game_time', 'sum', 'CRYSTALCAVES'),
     ('game_time', 'sum', 'MAGMAPEAK'), ('game_time', 'sum', 'NONE'), ('game_time', 'sum', 'TREETOPCITY'),
     'title', 'accuracy_group']]

from sklearn.metrics import cohen_kappa_score


def run_lgb(reduce_train, reduce_test):
    kf = KFold(n_splits=10)
    features = [i for i in reduce_train.columns if i not in ['accuracy_group', 'installation_id']]
    target = 'accuracy_group'
    oof_pred = np.zeros((len(reduce_train), 4))
    y_pred = np.zeros((len(reduce_test), 4))
    for fold, (tr_ind, val_ind) in enumerate(kf.split(reduce_train)):
        print('Fold {}'.format(fold + 1))
        x_train, x_val = reduce_train[features].iloc[tr_ind], reduce_train[features].iloc[val_ind]
        y_train, y_val = reduce_train[target][tr_ind], reduce_train[target][val_ind]
        train_set = lgb.Dataset(x_train, y_train, categorical_feature=categoricals)
        val_set = lgb.Dataset(x_val, y_val, categorical_feature=categoricals)

        params = {
            'learning_rate': 0.01,
            'metric': 'multiclass',
            'is_unbalance': True,
            'num_leaves': 31,
            'objective': 'multiclass',
            'num_classes': 4,
            'feature_fraction': 0.9,
            'subsample': 0.9
        }

        model = lgb.train(params, train_set, num_boost_round=100000, early_stopping_rounds=100,
                          valid_sets=[train_set, val_set], verbose_eval=100)
        oof_pred[val_ind] = model.predict(x_val)
        print('choen_kappa_score :',
              cohen_kappa_score(np.argmax(oof_pred[val_ind], axis=1), y_val, weights='quadratic'))
        y_pred += model.predict(reduce_test[features]) / 10
    return y_pred


def predict(y, coef=[0.5, 1.5, 2.5, 3.5]):
    # y = np.squeeze(y, axis=1)
    y[y < coef[0]] = 0
    y[(coef[0] < y) & (y < coef[1])] = 1
    y[(coef[1] < y) & (y < coef[2])] = 2
    y[(coef[2] < y) & (y < coef[3])] = 3
    y[coef[3] < y] = 4
    return y


def run_lgb_regression(reduce_train, reduce_test):
    kf = KFold(n_splits=10)
    features = [i for i in reduce_train.columns if i not in ['accuracy_group', 'installation_id']]
    target = 'accuracy_group'
    oof_pred = np.zeros((len(reduce_train), 1))
    y_pred = np.zeros((len(reduce_test), 1))
    for fold, (tr_ind, val_ind) in enumerate(kf.split(reduce_train)):
        print('Fold {}'.format(fold + 1))
        x_train, x_val = reduce_train[features].iloc[tr_ind], reduce_train[features].iloc[val_ind]
        y_train, y_val = reduce_train[target][tr_ind], reduce_train[target][val_ind]
        train_set = lgb.Dataset(x_train, y_train, categorical_feature=categoricals)
        val_set = lgb.Dataset(x_val, y_val, categorical_feature=categoricals)

        params = {
            'boosting_type': 'gbdt',
            'objective': 'regression',
            'metric': {'l2', 'l1'},
            'num_leaves': 31,
            'learning_rate': 0.05,
            'feature_fraction': 0.9,
            'bagging_fraction': 0.8,
            'bagging_freq': 5,
            'verbose': 0
        }

        model = lgb.train(params, train_set, num_boost_round=100000, early_stopping_rounds=100,
                          valid_sets=[train_set, val_set], verbose_eval=100)
        oof_pred[val_ind] = np.array(model.predict(x_val)).reshape(-1, 1)

        print('choen_kappa_score :',
              cohen_kappa_score(predict(oof_pred[val_ind].copy()), y_val, weights='quadratic'))
        y_pred += np.array(model.predict(reduce_test[features])).reshape(-1, 1) / 10
    return y_pred


def statistical_accuracy_group(reduce_test, y_pred):
    reduce = reduce_test.copy()
    reduce = reduce[['installation_id']]
    reduce['accuracy_group'] = y_pred.argmax(axis=1)
    print(reduce['accuracy_group'].value_counts(normalize=True))


def run_xgb(reduce_train, reduce_test):
    kf = KFold(n_splits=10)
    features = [i for i in reduce_train.columns if i not in ['accuracy_group', 'installation_id']]
    target = 'accuracy_group'
    # oof_pred = np.zeros((len(reduce_train), 4))
    y_pred = np.zeros((len(reduce_test), 4))

    pars = {
        'colsample_bytree': 0.5,
        'learning_rate': 0.01,
        'max_depth': 10,
        'subsample': 0.5,
        'objective': 'multi:softprob',
        'num_class': 4
    }

    for fold, (tr_ind, val_ind) in enumerate(kf.split(reduce_train)):
        print('Fold {}'.format(fold + 1))
        x_train, x_val = reduce_train[features].iloc[tr_ind], reduce_train[features].iloc[val_ind]
        y_train, y_val = reduce_train[target][tr_ind], reduce_train[target][val_ind]

        xgb_train = xgb.DMatrix(x_train, y_train)
        xgb_eval = xgb.DMatrix(x_val, y_val)

        xgb_model = xgb.train(pars,
                              xgb_train,
                              num_boost_round=10000,
                              evals=[(xgb_train, 'train'), (xgb_eval, 'val')],
                              verbose_eval=False,
                              early_stopping_rounds=100
                              )
        val_X = xgb.DMatrix(x_val)
        pred_val = [x for x in xgb_model.predict(val_X)]
        print('choen_kappa_score :', cohen_kappa_score(np.argmax(pred_val, axis=1), y_val, weights='quadratic'))
        test_x = xgb.DMatrix(reduce_test[features])
        y_pred += xgb_model.predict(test_x) / 10
    return y_pred


y_pred = run_lgb_regression(reduce_train, reduce_test)
statistical_accuracy_group(reduce_test, y_pred)
